// Carte pour afficher/supprimer des pharmacies

import {getPharmaFromApi} from '../Api/ApiPharma'; // import axios method get pour afficher les pharmacies
import {deletePharmaFromApi} from '../Api/ApiPharma'; // import axios method delete pour supprimer des pharmacies
import CartPut from './CartPut'; // import du component pour faire passer des props
import React from 'react';
import '../styles/Cart.css';

class Cart extends React.Component{
  constructor(props){
    super(props)
    console.log(props.pharmaShow, 'this is lola');
    this.state = {
      pharmacies: [],
      modifier: 0,
      pharmaShow : props.pharmaShow
    }
   
    this.deleteMethod = this.deleteMethod.bind(this);
    this.openForm = this.openForm.bind(this);
    this.closeForm = this.closeForm.bind(this);
    this.closeFinish = this.closeFinish.bind(this);
  }

  componentDidMount(){
    
    console.log("componentDidMount");
    getPharmaFromApi(this.props).then(pharmapi => {
      this.setState({
        pharmacies : pharmapi.data
      });
    });
  }
    
  deleteMethod(id){
    deletePharmaFromApi(id) 
    .then(() => alert("suppression de la pharmacie avec l'id : "+ id))
    .catch((error) => console.error(error, 'error'));
    this.props.onFinish()
  }

  openForm(id) {
    this.setState({ modifier: id});
  }

  closeForm() {
    this.setState({ modifier: 0});
  
    alert('La modification a été prise en compte');
    this.props.onFinish();
  }

  closeFinish() {
    this.setState({ modifier: 0});
  }

  render(){
    const { modifier, pharmaShow } = this.state;
    return (
      <article class="cart-fond" >
        <h2>Liste des pharmacies</h2>
        <ul class="cart-list">
          {this.state.pharmacies.map(pharmacie => (
            <li key={pharmacie.id}>
              <ul class="cart-list">
                <li>{pharmacie.nom}</li>
                <li>{pharmacie.quartier}</li>
                <li>{pharmacie.ville}</li>
                <li>{pharmacie.garde}</li>
                <button class="selector-button-supprimer" onClick={() => this.deleteMethod(pharmacie.id)} >Supprimer</button>
                <button onClick={() => this.openForm(pharmacie.id)}>Modifier</button>
              </ul>
              {modifier === pharmacie.id && <CartPut pharmacie={pharmacie} pharmaShow={pharmaShow} onClose={this.closeForm} onCloseForm={this.closeFinish} deleteMethod={this.closeFinish} />}
            </li>
          ))}
        </ul>
      </article>
    )
  }
}

export default Cart;