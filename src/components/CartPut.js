// Cart pour modifier une pharmacie

import { putPharmaFromApi } from '../Api/ApiPharma';
import React from 'react';
import '../styles/Cart.css';

class CartPut extends React.Component{
    constructor(props){
        super(props)
        console.log(props.pharmacie, 'this is marcel');
        this.state = props.pharmacie;
        

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleChange = (event) => {
        this.setState({ [event.target.name] :event.target.value});
    }

    handleClose = (event) => {
        event.preventDefault();
        this.props.onClose();
    }
    handleCloseForm = (event) => {
        event.preventDefault();
        this.props.onCloseForm();
    }

    handleSubmit = event => {
        event.preventDefault();

        console.log(typeof this.state);
        console.log(this.state);
 
        putPharmaFromApi(this.state)
            .then(res => {
                console.log(res, 'resultat');
                console.log(res.data);
                this.props.onClose();
            
                console.log(this.props.pharmaShow, "c'est ici");
            })
            .catch((error) => console.error(error, 'error'));
    }

    render(){
       
        return (
            <form onSubmit={this.handleSubmit}>
                {/* <label>
                id :
                <input type="text" value={this.state.id} name="id" onChange={this.handleChange}  />
                </label> */}
                <label>
                    nom :
                    <input type="text" value={this.state.nom} name="nom" onChange={this.handleChange}  />
                </label>
                <label>
                    quartier :
                    <input type="text" value={this.state.quartier} name="quartier" onChange={this.handleChange}  />
                </label>
                <label>
                    ville :
                    <input type="text" value={this.state.ville} name="ville" onChange={this.handleChange}  />
                </label>
                <label>
                    garde :
                    <input type="text" value={this.state.garde} name="garde" onChange={this.handleChange}  />
                </label>
                
                <input type="submit" value="Envoyer" />
                <button onClick={this.handleCloseForm}>Fermer</button>
            </form>
        )
    }
}

export default CartPut;