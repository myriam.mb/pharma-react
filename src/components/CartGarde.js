// Carte pour afficher la pharmacie de garde

import { getPharmaGarde } from '../Api/ApiPharma';
import React from 'react';
import '../styles/Cart.css';

class CartGarde extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      pharmacies: []
    };
  }

  componentDidMount(){
    console.log("componentDidMount");
    getPharmaGarde(this.props).then(pharmapi => {
      this.setState({
        pharmacies : pharmapi.data
      });
    });
  }

  render(){
    return (
      <article>
        <h2>Pharmacie de garde aujourd'hui</h2>
        <ul class="cart-list">
          {this.state.pharmacies.map(pharmacie => (
            <li key={pharmacie.id}>
                <ul class="cart-list">
                  <li>{pharmacie.nom}</li>
                  <li>{pharmacie.quartier}</li>
                  <li>{pharmacie.ville}</li>
                  <li>{pharmacie.garde}</li>
                </ul>
            </li>
          ))}
        </ul>
      </article>
    )
  }
}

export default CartGarde;