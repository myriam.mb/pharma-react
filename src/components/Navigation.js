// Fichier qui regroupe les boutons de navigation

import React from 'react';
import CartGarde from './CartGarde'; // carte de l'affichage de la pharmacie de garde
import Cart from './Cart'; // carte principal qui affiche la liste des pharmacies
import CartPost from './CartPost';
import '../styles/Nav.css';

class Nav extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      pharmacies: [],
      pharmaShow: false,
      pharmaGardeShow: false,
      pharmaAddShow: false,
    };
    this.hideComponent = this.hideComponent.bind(this);
    this.onRecup = this.onRecup.bind(this);
    this.onAdd = this.onAdd.bind(this); // pour bind la méthode onAdd
  }

  hideComponent(pharmacies) { // pour afficher ou masquer l'affichage selon le statut des boutons
    console.log(pharmacies);
    switch (pharmacies) {
      case "pharmaShow":
        this.setState({ pharmaShow: !this.state.pharmaShow, pharmaGardeShow:false, pharmaAddShow:false});
        break;  
      case "pharmaGardeShow":
        this.setState({ pharmaGardeShow: !this.state.pharmaGardeShow, pharmaShow:false, pharmaAddShow:false });
        break;
      case "pharmaAddShow":
        this.setState({ pharmaAddShow: !this.state.pharmaAddShow, pharmaShow:false, pharmaGardeShow:false });
        break;
      default:
      return(
        null
      );
    }
  }

  onRecup() { // pour finaliser la CartPut (modifier une pharmacie)
    this.setState({
      pharmaShow: !this.state.pharmaShow});
  }

  onAdd() { // pour finaliser la CartPost (ajouter une pharmacie)
    this.setState({
      pharmaAddShow: !this.state.pharmaAddShow});
  }

  render(){
    const { pharmaShow, pharmaGardeShow, pharmaAddShow } = this.state;
    return (
      <div class="nav-taille" >
          <div>
            <button class="selector-button-nav" onClick={() => this.hideComponent("pharmaShow")}>
                Listes des pharmacies
            </button>
            <button class="selector-button-nav" onClick={() => this.hideComponent("pharmaGardeShow")}>
                pharmacie de garde
            </button>
            <button class="selector-button-nav" onClick={() => this.hideComponent("pharmaAddShow")}>
                Ajouter une pharmacie
            </button>
          </div>
          {/* pour passer les props vers d'autres components */}
          {pharmaShow && <Cart pharmaShow={pharmaShow}  onFinish={this.onRecup}/>} 
          {pharmaGardeShow && <CartGarde />}
          {pharmaAddShow && <CartPost pharmaAddShow={pharmaAddShow} onAddFinish={this.onAdd} />}
      </div>
    )
  }
}

export default Nav;