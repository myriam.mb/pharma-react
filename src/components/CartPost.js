// Carte pour ajouter une pharmacie

import { postPharmaFromApi } from '../Api/ApiPharma';
import React from 'react';
import '../styles/Cart.css';

class CartPost extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      nom:'',
      quartier:'',
      ville:'',
      garde: '',
      pharmaAddShow : props.pharmaAddShow
    };
    
  }
    
  handleChange = (event) => {
    this.setState({ [event.target.name] :event.target.value});
  }

  handleSubmit = event => {
    event.preventDefault();

    console.log(typeof this.state);
    console.log(this.state);

    postPharmaFromApi(this.state);
    this.setState({
      nom: '',
      quartier: '',
      ville: '',
      garde: ''
    });
    alert('Pharmacie ajouté');
    this.props.onAddFinish(); //appelle la méthode onAddFinish du composant Navigation pour fermer le formulaire
  }

  render(){
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Nom :
          <input type="text" value={this.state.nom} placeholder="ex : Pharmacie" name="nom" onChange={this.handleChange} />
        </label>
        <label>
          Quartier :
          <input type="text" value={this.state.quartier} placeholder="ex : Jean-jaurés" name="quartier" onChange={this.handleChange} />
        </label>
        <label>
          ville :
          <input type="text" value={this.state.ville} placeholder="ex : Toulouse" name="ville" onChange={this.handleChange} />
        </label>
        <label>
          garde :
          <input type="text" value={this.state.garde} placeholder="jours de la semaine ex : lundi" name="garde" onChange={this.handleChange} />
        </label>
        
        <input class="cartPost-selector" type="submit" value="Envoyer" />
      </form>
    )
  }
}

export default CartPost;