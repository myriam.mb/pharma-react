// La banière

import '../styles/Banner.css';
import logo from '../assets/feuille-min.svg';

function Banner() {

    return <div class="title-banner">
                <img src={logo} alt='pharmacie' className='title-logo' />
                <h1 class="title-banner-text" >Pharmapi Client en React</h1>
            </div>

}

export default Banner;
