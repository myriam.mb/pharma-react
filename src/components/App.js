// Page principale 

import Banner from './Banner';
import Nav from './Navigation';
import '../styles/App.css';

function App() {
    return (
      <div class="app-back">
        <Banner />
        <Nav />
      </div>
    );
}

export default App;

