// Fichier pour l'API Symfony

import axios from 'axios';

// Pour afficher la liste des pharmacies
export function getPharmaFromApi(){
  const url = 'http://127.0.0.1:8000/pharma';
  return axios.get(url);
}

// Pour afficher la pharmacie de garde aujourd'hui
export function getPharmaGarde() {
  return axios.get('http://127.0.0.1:8000/pharma-garde');
}

//Pour ajouter une pharmacie
export function postPharmaFromApi(marcel) { 
  axios.post('http://127.0.0.1:8000/pharma', {
    nom : marcel.nom,
    quartier: marcel.quartier,
    ville: marcel.ville,
    garde:marcel.garde
  })
  .then(res => {
    console.log(res, 'resultat');
    console.log(res.data);
  })
  .catch((error) => console.error(error, 'error'));
}

// Pour supprimer une pharmacie
export function deletePharmaFromApi(OKOKOK){
  return axios.delete('http://127.0.0.1:8000/pharma/' + OKOKOK)
}

// Pour modifier une pharmacie
export function putPharmaFromApi(marcel) { 
  return axios.put('http://127.0.0.1:8000/pharma/' + marcel.id , {
    nom : marcel.nom,
    quartier: marcel.quartier,
    ville: marcel.ville,
    garde:marcel.garde
  });
}



 